<?php
namespace GoogleGeocodingApi\Traits;

trait CommonTrait
{
    /**
     * Returns an array of public attributes
     *
     * @return array
     */
    public function getAttributes()
    {
        $attributes = array();

        $obj = new \ReflectionObject($this);
        $reflectionProperties = $obj->getProperties(\ReflectionProperty::IS_PUBLIC);
        foreach ($reflectionProperties as $reflectionProperty) {
            $attributes[$reflectionProperty->getName()] = $reflectionProperty->getValue($this);
        }

        return $attributes;
    }

    /**
     * Sets the properties of an object by an array of attributes.
     *
     * @param array $attributes
     * @return $this
     */
    public function setAttributes(array $attributes)
    {
        foreach ($attributes as $property => $value) {
            if (property_exists($this, $property) == true) {
                $this->{$property} = $value;
            }
        }

        return $this;
    }

    /**
     * Returns an array of values of a given property from an array of models.
     *
     * @param $models
     * @param $field
     * @return array
     */
    public function getFieldValues($models, $field)
    {
        $values = array();
        foreach ($models as $model) {
            $values[] = $model->$field;
        }

        return $values;
    }

}