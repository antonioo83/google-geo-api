<?php
require_once "vendor/autoload.php";

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$dbParams = file_get_contents('config/database-config.php');
$config = Setup::createAnnotationMetadataConfiguration(
    array(__DIR__ . "/src"),
    true,
    null,
    null,
    false
);
$entityManager = EntityManager::create($dbParams, $config);
